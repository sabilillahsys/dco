-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Sep 2020 pada 08.04
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newnormal`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2020_09_12_103128_create_roles_table', 1),
(5, '2020_09_12_103128_create_users_table', 1),
(6, '2020_09_26_183400_create_postings_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `postings`
--

CREATE TABLE `postings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `judul` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `postings`
--

INSERT INTO `postings` (`id`, `user_id`, `judul`, `isi`, `pic`, `created_at`, `updated_at`) VALUES
(2, 1, 'TIPS BELANJA ERA NEW NOEMAL', '<p>Berbelanja kebutuhan rumah tangga merupakan salah satu kebutuhan pokok baik dalam masa normal atau pandemi covid-19 seperti saat sekarang ini. Tapi saat ini Anda tak bisa sesering biasanya untuk berbelanja kebutuhan, melainkan belanja sekali untuk beberapa hari bahkan untuk keperluan seminggu. Tips belanja nyaman dan sehat di masa pandemi covid-19 ini. Perhatikan beberapa hal di bawah ini kala Anda berbelanja.</p>\r\n\r\n<p>1. Cuci tangan dan hand sanitizer Di banyak tempat atau toko berbelanja menyediakan hand sanitizer yang mudah dijangkau. Manfaatkan sesering mungkin setelah Anda memegang dan memilih produk. Walau telah diterapkan disinfektan secara berkala untuk tempat-tempat yang rentan di banyak supermarket termasuk di Hero Supermarket, tidak ada salahnya memastikan pembersihan tangan dengan hand sanitizer dan mencuci tangan di tempat yang telah disediakan.</p>\r\n\r\n<p>2. Gunakan masker Selalu gunakan masker setiap Anda keluar rumah, termasuk saat Anda berbelanja ke supermarket.</p>\r\n\r\n<p>3. Jaga jarak aman Toko atau supermarket telah menyediakan petunjuk untuk mengantre berjarak sebagai bagian dari protokol kesehatan covid-19. Perhatikan selalu jarak aman Anda dengan pengunjung lainnya, dengan begitu Anda meminimalisir kontaminasi.</p>\r\n\r\n<p>4. Berbelanja aman dengan cashless Usahakan melakukan pembayaran dengan debit untuk mengurangi kontak langsung dengan uang kertas.</p>\r\n\r\n<p>5. Catat belanja kebutuhan bulanan Daftar belanja bulanan akan membantu Anda meminimalisir waktu memilih produk.</p>\r\n\r\n<p>6. Gunakan Whatsapp Delivery Berbagai toko dan supermarket juga memberikan kemudahan lainnya untuk Anda yang tidak dapat keluar rumah dengan berbelanja melalui Whatsapp. Anda dapat memilih produk yang dibutuhkan dan barang-barang belanja akan diantar langsung ke rumah.</p>\r\n\r\n<p>7. Cek harga atau diskon Gunakan gadget Anda untuk melihat-lihat apakah ada potongan harga atau diskon atau pilihan menarik. Hal ini bisa jadi tawaran tambahan saat Anda berbelanja. Misalnya seperti yang dilakukan oleh Giant dengan program Harga Teman, yaitu sembako murah setiap hari melalui paket sembako yang dibanderol dengan harga menarik.</p>', 'public/pic/mjZ0usdAtCPmd6BCYjAIUFUNmV6kmmsD5UXUcAXo.png', '2020-09-26 19:36:08', '2020-09-26 19:38:19'),
(3, 1, 'Cara Efektif Bagi Orang Tua untuk Mendampingi Anak', 'Sebagian besar orang tua mungkin masih cemas jika sekolah mulai dibuka saat new normal diberlakukan. Bahkan sejumlah ahli dari kalangan perlindungan anak dan dokter anak di Indonesia pun menyarankan bahwa sebaiknya anak-anak tetap berada di rumah untuk belajar di era new normal dengan modifikasi pengajaran yang diberlakukan sekolah. Ini artinya, anak-anak masih akan belajar di era new normal lewat metode dalam jaringan (daring). Untuk menjalankan kegiatan belajar di era new normal tersebut, tentu diperlukan peran orang tua untuk mendampingi anak agar mereka tetap fokus belajar di rumah. Berikut kami sajikan beberapa tips mendampingi anak selama belajar di era new normal.\r\nCara Agar Anak Tetap Fokus Selama Belajar di Era New Normal\r\nBeberapa waktu terakhir, anak dan orang tua sudah menjalani masa belajar di rumah. Tentu orang tua dapat melihat skema Si Kecil, apakah mereka bisa belajar mandiri, mudah bosan, membutuhkan lebih banyak bimbingan selama pelajaran, atau mudah teralihkan perhatiannya dan membutuhkan ruangan tertentu untuk dapat menyelesaikan pelajaran dengan tenang. \r\nJika skema tersebut sudah orang tua ketahui, dari situ dapat dibuat strategi pembelajaran terbaik untuk anak. Orang tua dapat mulai mengembangkan pendekatan pembelajaran yang lebih baik dan memudahkan anak-anak. Pastikan anak memiliki ruang dan utilitas yang memadai selama belajar di era new normal untuk menyesuaikan waktu pendidikan dengan kebutuhan mereka sendiri, dan meminimalisir rasa frustasi atau rasa bosan sebanyak mungkin. \r\nSelain itu, cara untuk meningkatkan motivasi agar anak tetap fokus belajar meliputi:\r\n\r\nPertahankan Rutinitas Harian yang Terstruktur\r\n\r\nSebaiknya tetap pertahankan rutinitas harian yang terstruktur, tetapi masih fleksibel. Anak-anak cenderung melakukan yang terbaik dengan rutinitas. Namun, usahakan untuk tidak membuatnya terlalu kaku. Hal ini dapat menyebabkan stres tambahan jika orang tua mencoba memaksakan belajar pada waktu yang tidak optimal untuk anak. \r\n\r\nManfaatkan Sumber Daya Online\r\n\r\nTerkadang diperlukan pendekatan yang berbeda. Jadi, manfaatkan sumber daya online seperti tur pendidikan gratis, kegiatan yang menyenangkan, atau bahkan sesi obrolan grup. Dengan begitu, itu memungkinkan mereka dapat tetap berinteraksi dengan teman sebaya dan sekelas mereka untuk berbicara tentang apa yang mereka pelajari atau lakukan.\r\n\r\nRencanakan Proyek Pembelajaran yang Tidak Konvensional\r\n\r\nJika orang tua merasa anak lelah dan bosan dengan kegiatan sehari-hari, cobalah mengubah keadaan. Pikirkan tentang minat mereka dan temukan cara untuk membuat mereka tetap mempelajari sesuatu. Misalnya ajak mereka untuk memasak kue di dapur, bermain puzzle, atau blok bangunan agar mereka tidak terlalu lama menatap layar gadget. \r\nOrang tua juga bisa mencari kelas dan aktivitas ekstrakurikuler secara online agar mereka lebih banyak bergerak. Semua ini adalah cara agar anak dapat fokus mengikuti arahan serta memanfaatkan kreativitas mereka.\r\n\r\nJelaskan Bahwa Belajar di Era New Normal Bukanlah Suatu Liburan Panjang\r\n\r\nMeskipun belajar di rumah, anak mungkin merasa seperti liburan. Situasi inilah yang mungkin membuat anak jadi sulit untuk fokus saat belajar dari rumah. Ingatkan kembali bahwa sebenarnya mereka tidak sedang berlibur. Jelaskan bahwa tugas, nilai, tes, dan ujian tetap berlangsung karena sekolah beralih menjadi online. \r\n\r\nJangan Lupa untuk Mengajak Anak Bersenang-senang\r\n\r\nJika waktu belajar mereka telah selesai atau sedang hari libur, orang tua dapat merencanakan kegiatan tanpa melihat layar gadget yang tidak kalah menyenangkan. Mungkin sebelum pandemi ini, orang tua jarang memiliki waktu berkualitas bersama anak. Jadi, manfaatkan masa-masa seperti saat ini untuk membangun ikatan dengan bermain atau beraktivitas yang menyenangkan bagi anak di rumah. Misalnya menonton film di TV, bermain tebak-tebakan, bermain ular tangga atau monopoli, dan masih banyak pilihan kegiatan konvensional lainnya. \r\nDemikian beberapa cara yang bisa Anda terapkan untuk dampingi anak selama belajar di era new normal. Dengan demikian, dapat disimpulkan bahwa faktor psikologis anak juga perlu menjadi perhatian yang tidak boleh dikesampingkan selama belajar di era new normal. Tetaplah dampingi dan selalu perhatikan kesehatan fisik sang buah hati, ya!\r\n\r\nhttps://www.kelaspintar.id/', 'public/pic/3Ll0IjxBAeDXVDqvDJtw70lKkkHyH4tgMeT7XNuO.png', '2020-09-26 19:43:40', '2020-09-26 19:43:40'),
(4, 1, 'Buat Sendiri Hand Sanitizer Di Rumah', 'Membuat hand sanitizer homemade dengan menggunakan bahan-bahan antara lain alkohol, aloevera, dan perasan air jeruk nipis. Tetap memperhatikan anjuran sesuai standart yang sesuai dan menjaga kebersihan agar hasilnya tetap steril. \r\nBahan dasar yang digunakan yakni memanfaatkan lidah buaya sudah lama dikenal sabagai tanaman yang kaya akan manfaat. Tak hanya untuk rambut, sekarang aloevera banyak diformulasikan untuk perawatan kulit. Manfaat Aloevera itu sendiri untuk menangkal virus corona covid-19, salah satunya bisa diperoleh dengan mengolah tanaman ini menjadi hand sanitizer.\r\nTekstur lidah buaya yang berwujud gel lembut sangat cocok dan fleksibel untuk diolah menjadi hand sanitizer. Dengan lidah buaya, kalian bisa membuat hand sanitizer yang cair ataupun sedikit padat seperti gel. Lidah buaya mempunyai sifat anti-inflamasi dan anti-bakteri, sehingga cocok untuk digunakan sebagai bahan pembersih permukaan kulit, seperti hand sanitizer.\r\nSenyawa dalam lidah buaya tidak hanya bisa membersihkan tangan dari bakteri dan virus, namun juga bisa membuat tangan jadi lebih lembab. Dengan begitu, hand sanitizer dari lidah buaya kan relatif lebih aman jika dibandingkan dengan hand sanitizer pada umumnya yang didominasi bahan kimia. Berikut merupakan proses pembutan dari hand sanitizer homemade.\r\n\r\nBahan-Bahan:\r\n-	Alkohol 70%\r\n-	Lidah buaya\r\n-	Jeruk Nipis\r\n-	Air Bersih \r\nAlat:\r\n-	Mangkok\r\n-	Sutil\r\n-	Blender\r\n-	saringan\r\n-	Corong Takaran\r\n-	Botol kosong 60ml\r\n-	Corong Krim \r\nCara Pembuatan:\r\n-	Langkah awal, blender bagian dalam lidah buaya yang sudah di kupas dan saring menggunakan kain bersih\r\n-	Langkah selanjutnya yaitu siapkan wadah/mangkok dan sutil, kemudian campurkan alkohol 70% dengan aloevera gel dengan perbandingan 1:2 \r\n-	Aloevera 100ml an alkohol 200ml. Jika ingin lebih encer, anda boleh pakai perbandingan 1:3\r\n-	Kemudian langkah selanjutnya tambahkan perasan jeruk nipis secukupnya, disini saya sebagai pengganti Air rebusan Daun Sirih.\r\n-	Lalu aduk merata semua campuran bahan-bahan tersebut.\r\n-	Setelah bahan-bahan tadi selesai diracik, jangn lupa kita uji coba hasil racikan kita apakah hasilnya aman untuk digunakan.\r\n-	Disini kami menguji dengan mengoleskan/menggunakan dengan tangan kami sendiri apakah hasilnya aman/tidak ketika dipakai di kulit. (Tidak terasa gatal-gatal/kulit memerah)', 'public/pic/rcWvmhnde130xmcfHmt8E8evd684WnNHxBA1gNap.png', '2020-09-26 19:55:35', '2020-09-26 19:55:35'),
(5, 1, 'Membuat Face Shield Sederhana', 'Upaya pencegahan penularan Covid-19 perlu dilakukan dan dimulai dari diri sendiri. Dengan pola hidup sehat seperti berolah raga, makan makanan bergizi, mencuci tangan sebelum makan dan menggunakan masker saat keluar rumah sesuai dengan petunjuk WHO. Namun corona bukan hanya dapat menular lewar saluran pernafasan akan tettapi juga bisa menular lewat mata. Masker hanya menutupi mulutt dan hidung tidak melindungi mata sehingga perlu alat APD tambahan yaitu pelindung wajah atau Face Shield.\r\nFace Shield adalah alat pelindung wajah mirip tameng yang terbuat  plastik atau mika. Dengan menggunakan bahan dan alat yang sederhana dan mudah diperolah, kita bisa membuat Face Shield sendiri dengan tidak mengeluarkan baya yang begitu mahal.\r\nTata cara pembuatan face shield dengn alat dan bahan sebagai berikut :\r\n	Bahan :\r\n-	Mika\r\n-	Tali Rafia\r\n-	Karton bekas\r\n	Alat :\r\n-	Gunting\r\n-	Ballpoint \r\n-	Steples\r\n-	Penggaris\r\nLangkah pembuatan yaitu :\r\n1.	Buat pola persegi panjang pada karton bekas dengan panjang 20cm x lebar 4cm kemudian digunting mengikuti pola. \r\n\r\n2.	Lipat mika hingg menutupi ke dua sisi karton. \r\n\r\n3.	Rekatkan mika dan karton dengan menggunakan steples agar karton dan mika tidak terlepas. \r\n\r\n4.	Lubangi disisi kanan dan kiri sebagai tempat untuk bisa dipasang tali \r\n\r\n5.	Berikan tali rafia pada sisi yang telah dilubangi..\r\n\r\n6.	Face Shield siap digunakan.\r\n \r\nVideo dapat dilihat pada Channel Youtube :\r\nhttps://youtu.be/ffX5Nqehbkk', 'public/pic/oOUqgTiOFgyeJuFMWJVwAb4jNgq2ChcmIo1FLSNb.png', '2020-09-26 20:03:28', '2020-09-26 20:03:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Digunakan sebagai Role Admin', NULL, NULL),
(2, 'Guru', 'Digunakan sebagai Role Guru', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `nama` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sekolah` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_sekolah` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `nama`, `alamat`, `email`, `password`, `hp`, `sekolah`, `alamat_sekolah`, `pic`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', NULL, 'admin@mail.com', '$2y$10$AXToTcu4tCI/EhnhHVF06e13fdEYf9JH5RcK4MJPOvPk/s3EuIjPq', NULL, NULL, NULL, 'public/profile/5jG0WiuqOHIrogaLfwEOhdPlSw6q8V90pl1dZExm.png', 'AKTIF', NULL, NULL, '2020-09-26 22:37:36'),
(2, 1, 'Kamaluddin', 'kamal', 'kamal@mail.com', '$2y$10$hkFSMrrVGK6/8c3BjYFxOeel9Ln54lTsfIQIFamVVh6dsWnC/Q4Au', '085208985534', NULL, NULL, NULL, 'AKTIF', NULL, '2020-09-26 22:55:06', '2020-09-26 22:55:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `postings`
--
ALTER TABLE `postings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postings_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `postings`
--
ALTER TABLE `postings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `postings`
--
ALTER TABLE `postings`
  ADD CONSTRAINT `postings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
