<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@if(session()->get('success')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                        const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                                })

                                Toast.fire({
                                icon: "success",
                                title: "{{session()->get('success')}}"
                                })
                         }); 
                </script>
                @elseif(session()->get('gagal')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                        const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                                })

                                Toast.fire({
                                icon: "error",
                                title: "{{session()->get('gagal')}}"
                                })
                         }); 
                </script>
                @elseif(session()->get('gagaldaftar')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                            swal("Maaf NIM Tidak Valid", "{{session()->get('gagaldaftar')}}", "error");
                         }); 
                </script>
                @elseif(session()->get('hapus')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                            swal("Sukses Hapus Data", "{{session()->get('hapus')}}", "success");
                         }); 
                </script>
                @elseif(session()->get('tambah')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                            swal("Sukses Tambah Data", "{{session()->get('tambah')}}", "success");
                         }); 
                </script>
                @elseif(session()->get('edit')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                            swal("Sukses Edit Data", "{{session()->get('edit')}}", "success");
                         }); 
                </script>
                @elseif(session()->get('update_benar')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                            swal("Sukses", "{{session()->get('update_benar')}}", "success");
                         }); 
                </script>
                @elseif(session()->get('update_salah')) 
                <script type="text/javascript">
                    $(document).ready(function(){ 
                            swal("Gagal", "{{session()->get('update_salah')}}", "error");
                         }); 
                </script>
              @endif