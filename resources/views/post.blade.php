@extends('layout')

@section('content')
<main id="main">
    <div class="row" style="margin: 0px;">
        <div class="col-md-8" style="margin-top: 15px; margin-bottom: 15px;">
            <div class="card border-white border rounded shadow"><img class="card-img-top w-100 d-block" src="{{url('/')}}/{{Storage::url($data->pic)}}" />
                <div class="card-body">
                    <h1 class="card-title">{{$data->judul}}</h1>
                    <p>{!!$data->isi!!}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 15px;">
        <div class="row" style="margin: 0px;">
        @foreach($data_all as $datas)
        <div class="col-md-12" style="margin-top: 15px;">
            <div class="card border-white border rounded shadow"><img class="card-img-top w-100 d-block" src="{{url('/')}}/{{Storage::url($datas->pic)}}" />
                <div class="card-body">
                    <h4 class="card-title">{{$datas->judul}}</h4><a href="{{route('view_post',$datas->id)}}" class="btn btn-warning text-white" role="button"><strong>Baca Selengkapnya</strong></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
        </div>
    </div>
</main>
@endsection