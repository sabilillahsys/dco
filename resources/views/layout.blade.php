<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>DCO | Dokter Covid 19 Online</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="covid19,corona,new normal, kembali beraktifitas, Dokter COVID Online, DCO">
  <meta content="" name="Dokter COVID Online (DCO) Merupakan Aplikasi berbasis web yang menyajikan tips dan informasi tentang Covid 19 dan New Normal Terbaru.">
  <meta name="theme-color" content="#1C4FC3">
  <!-- Favicons -->
  <link href="{{asset('img/logo.svg')}}" rel="icon">
  <link href="{{asset('img/apple-touch-icon.svg')}}" rel="apple-touch-icon">
  @include('asset.css')
</head>

<body>
@include('asset.nav')
@include('asset.slider')
  @yield('content')
  @include('asset.footer')

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

@include('asset.js')
  

</body>
</html>
