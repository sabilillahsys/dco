<!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <img src="img/g1.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>DOKTER COVId ONLINE</h2>
        <div>
          <a href="#services" class="btn-services scrollto">Tip New Normal</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->