<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>DCO | Dokter Covid 19 Online</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
  @include('asset.css')
</head>

<body>
@include('asset.nav')
@include('asset.slider')
  @yield('content')
  @include('asset.footer')

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->
@include('asset.js')
  

</body>
</html>
