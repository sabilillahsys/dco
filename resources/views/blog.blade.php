@extends('layout')

@section('content')
<main id="main">
    <div class="row" style="margin-top: 15px; margin-bottom: 15px;">
        @foreach($data as $datas)
        <div class="col-md-4" style="margin-top: 15px;">
            <div class="card border-white border rounded shadow"><img class="card-img-top w-100 d-block" src="{{url('/')}}/{{Storage::url($datas->pic)}}" />
                <div class="card-body">
                    <h4 class="card-title">{{$datas->judul}}</h4><a href="{{route('view_post',$datas->id)}}" class="btn btn-warning text-white" role="button"><strong>Baca Selengkapnya</strong></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</main>
@endsection