<!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <a href="{{url('/')}}" class="scrollto"><img src="{{asset('img/logo.svg')}}" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="{{url('/')}}">Home</a></li>
          <li><a href="{{url('/')}}/#about">Tentang Covid</a></li>
          <li><a href="{{url('/')}}/#services">Tips</a></li>
          <li><a href="{{route('blog')}}">Blog</a></li>
          <li><a href="{{route('login')}}">Masuk</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->