<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>DCO</h3>
            <p>Dokter COVID Online (DCO) Merupakan Aplikasi berbasis web yang menyajikan tips dan informasi tentang Covid 19 dan New Normal Terbaru.</p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
                <li class="active"><a href="#intro">Home</a></li>
                <li><a href="#about">Tentang Covid</a></li>
                <li><a href="#services">Tips</a></li>
                <li><a href="#">Blog</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              Perumahan Tellang Asri <br>
              Jln. Raya tellang kamal 69162<br>
              Madura <br>
              <strong>Phone:</strong><a href="https://api.whatsapp.com/send?phone=6282338959678" target="_blank" rel="noopener noreferrer">+62 823-3895-9678</a><br>
              <strong>Email:</strong> info@doktercovidonline.com<br>
            </p>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>DCO</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=DCO
        -->
        Designed by <a href="{{url('/')}}">DCO</a>
      </div>
    </div>
  </footer><!-- #footer -->