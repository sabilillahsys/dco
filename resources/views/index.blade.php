@extends('layout')

@section('content')
<main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3><b>Tentang Covid</b></h3>
          </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
          <p>Penyakit virus corona (COVID-19) adalah penyakit menular yang disebabkan oleh virus corona yang baru-baru ini ditemukan.</p>
          <p>Sebagian besar orang yang tertular COVID-19 akan mengalami gejala sebagai berikut:</p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-thermometer-full"></i></div>
              <h4 class="title"><a href="">Demam</a></h4>
              <p class="description">Demam adalah kondisi meningkatnya suhu tubuh hingga lebih dari 38<sup>0</sup> C. Demam menandakan adanya penyakit atau kondisi lain di dalam tubuh.</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-plus-square"></i></div>
              <h4 class="title"><a href="">Batuk Kering</a></h4>
              <p class="description">Timbulnya sensasi menggelitik atau gatal di bagian tenggorokan akibat iritas, dokter sebut sebagai batuk tidak berdahak atau batuk nonproduktif</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-heartbeat"></i></div>
              <h4 class="title"><a href="">Sesak Nafas</a></h4>
              <p class="description">Nafas pendek atau yang disebut juga dengan dispnea merupakan kondisi di mana seseorang kesulitan untuk bernafas.</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="{{asset('img/g2.svg')}}" class="img-fluid" alt="">
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="{{asset('img/g3.svg')}}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4>Lindungi Dirimu Dari Penularan Corona Virus!</h4>
            <p>
            Penularan virus Corona juga dapat terjadi jika orang menghirup droplet yang keluar dari batuk atau napas (bersin) orang yang terjangkit virus Corona. Karena itu, penting bagi kita untuk menjaga jarak lebih dari 1 meter dari orang yang sakit.
            </p>
            <p>
              Jaga imunitas tubuh dan rajin olahraga setiap saat, penting sekali karena dengan imun yang kuat tubuh tidak gampang terserang penyakit termasuk Corona Virus.
            </p>
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="{{asset('img/about-extra-2.svg')}}" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <h4>Tetap beraktifitas di masa pandemi dengan ikuti protokol kesehatan</h4>
            <h5>1. Jaga kebersihan tangan</h5>
            <p>Bersihkan tangan dengan cairan pencuci tangan atau hand sanitizer, apabila permukaan tangan tidak terlihat kotor. Namun, apabila tangan kotor maka bersihkan menggunakan sabun dan air mengalir. Cara mencucinya pun harus sesuai dengan standar yang ada, yakni meliputi bagian dalam, punggung, sela-sela, dan ujung-ujung jari.</p>
            <h5>2. Jangan menyentuh wajah</h5>
            <p>Dalam kondisi tangan yang belum bersih, sebisa mungkin hindari menyentuh area wajah, khususnya mata, hidung, dan mulut. Mengapa? Tangan kita bisa jadi terdapat virus yang didapatkan dari aktivitas yang kita lakukan, jika tangan kotor ini digunakan untuk menyentuh wajah, khususnya di bagian yang sudah disebutkan sebelumnya, maka virus dapat dengan mudah masuk ke dalam tubuh.</p>
            <a href="https://www.kompas.com/tren/read/2020/05/18/103200465/simak-panduan-protokol-kesehatan-pencegahan-covid-19-untuk-sambut-new?page=all" class="btn-services scrollto" target="_blank">Baca Selengkapnya</a>  
          </div>
          
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>TIPS</h3>
          <p>Mau keluar rumah? Bahaya Covid dimana mana <span>#dirumahsaja</span>, tapi jangan khawatir keluar rumah lakukan tips ini:</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="">Pakai Masker</a></h4>
              <p class="description">Keluar rumah jarak dekat jangan jadi alasan gag pakek masker ya</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="fa fa-flask" style="color: #e9bf06;"></i></div>
              <h4 class="title"><a href="">Bawa Handsanitizer</a></h4>
              <p class="description">Penting nih gaes, bawa selalu Handsanitizer buat jaga-jaga aja takut gag ada air</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="fa fa-hand-spock-o" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="">Cuci Tangan</a></h4>
              <p class="description">Ya.. Cuci tangan yang bersih supaya virusnya mati apalagi sehabis bepergian sampek rumah langsung mandi ya..</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="fa fa-user-times" style="color:#41cf2e;"></i></div>
              <h4 class="title"><a href="">Jaga Jarak</a></h4>
              <p class="description">Jaga jarak bukan berati gag menghargai tapi dengan jaga jarak di masa pandemi menyelamatkan nyawa kita dan orang lain</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>Info COVID 19 Terbaru</h3>
          <p>Info COVID 19 diambil dari website covid19.co.id tanggal 27 September 2020</p>
        </header>

        <div class="row row-eq-height justify-content-center">

        </div>

        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">271.339</span>
            <p>Terkonfirmasi</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">61.628</span>
            <p>Kasus Aktif</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">199.403</span>
            <p>Sembuh</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">10.308</span>
            <p>Meninggal</p>
          </div>
  
        </div>

      </div>
    </section>

  </main>
@endsection