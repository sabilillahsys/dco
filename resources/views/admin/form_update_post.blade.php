@extends('admin.layout')

@section('content')

<form action="{{route('post.update_post',$data->id)}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="row">
<div class="container">
    <div class="col-md-8" >
        <div class="row">
            <div class="col-md-12" style="margin-bottom:20px;">
                
                <div class="form-group">
                    <input placeholder="Judul Posting" type="text" value="{{$data->judul}}" name="judul" class="form-control" required>
                </div>
                
                <textarea id="konten" class="form-control" name="isi" rows="10" cols="50" placeholder="Teori">{{$data->isi}}</textarea>
            </div>
            <div class="col-md-6" style="margin-bottom:20px;">
                <div class="card">
                    <div class="card-header">
                        <h6 class="text-uppercase mb-0">File / Gambar</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Gambar :</label>
                            <input type="file" name="pic" id="imgInp" class="form-control" >
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6">
        <div class="row">
            
            <div class="col-md-12" style="margin-bottom:20px;">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group" ">
                                <button type="submit" class="btn btn-block btn-success"> Kirim </button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
        </div>
    </div>
    
</div>
</div>
</form>
@endsection
@push('scripts')
<script>
  var konten = document.getElementById("konten");
    CKEDITOR.replace(konten,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>
@endpush