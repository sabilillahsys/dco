@extends('admin.layout')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Post</h1>
        <div class="card shadow mb-4">
            <div class="card-body">
                <a href="{{route('post.form_input_post')}}" class="btn btn-primary">Tambah Post</a>
            </div>
        </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tabel Data Post</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="admin" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>NO</th>
                <th>FOTO</th>
                <th>JUDUL</th>
                <th>PENGATURAN</th>
            </tr>
            </thead>
            
        </table>
        </div>
    </div>
    </div>
</div>
<!-- /.container-fluid -->

@endsection
@push('scripts')
<script>
$(function() {
    $('#admin').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{route('post.json_post')}}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'pic', name: 'pic' },
            { data: 'judul', name: 'judul' },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
    });
});
</script>

@endpush