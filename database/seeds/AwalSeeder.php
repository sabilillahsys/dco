<?php

use Illuminate\Database\Seeder;

class AwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => "Admin",
            'description' => "Digunakan sebagai Role Admin",
        ]);
        DB::table('roles')->insert([
            'name' => "Guru",
            'description' => "Digunakan sebagai Role Guru",
        ]);
        
        DB::table('users')->insert([
            'nama'=>"Admin", 
            'email'=>"admin@mail.com", 
            'password'=>'$2y$10$AXToTcu4tCI/EhnhHVF06e13fdEYf9JH5RcK4MJPOvPk/s3EuIjPq',
            'pic'=>"belum",
            'status'=>"AKTIF",
            'role_id'=>1,
        ]);
    }
}
