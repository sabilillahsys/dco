<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posting extends Model
{
    //
    protected $fillable = [
        'user_id', 
        'judul', 
        'isi',
        'pic',
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
