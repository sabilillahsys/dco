<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DataTables;
use Auth;
use Illuminate\Support\Facades\Storage;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $user=user::count();
        return view('admin.index',compact('user'));
    }
    public function data_admin()
    {
        return view('admin.data_admin');
    }
   
    public function json_admin()
    {
        $data=User::where('role_id','=','1')->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#edit'.$user->id.'" class="btn btn-primary">Update</button>
                        <!-- Modal-->
                        <div id="edit'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="exampleModalLabel" class="modal-title">Data Admin</h4>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <p>Silahkan tambhakan .</p>
                                    <form action="'.route('admin.update_user',$user->id).'" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <div class="form-group">
                                        <label>Nama *</label>
                                        <input type="text" name="nama" class="form-control" required value="'.$user->nama.'">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat *</label>
                                        <input type="text" name="alamat" class="form-control" required value="'.$user->alamat.'">
                                    </div>
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input type="email" name="email" class="form-control" required value="'.$user->email.'">
                                    </div>
                                    <div class="form-group">
                                        <label>Telephone *</label>
                                        <input type="number" name="hp" class="form-control" required value="'.$user->hp.'">
                                    </div>
                                    <div class="form-group">       
                                        <input type="submit" value="Update" class="btn btn-primary">
                                    </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        <a href="'.route('admin.delete_user',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                        ';
                    })
                    ->addColumn('pic', function ($user) {
                        $ub=$user->pic;
                        $url=url('/').Storage::url($user->pic);
                        $not= asset('img/logo.svg');
                        $kondisi="belum";
                        if($ub == $kondisi){
                            return '
                            <img src="'.$not.'" width="113px" alt="">
                            ';
                        }else{
                            return '
                            <img src="'.$url.'" width="113px" alt="">
                            ';
                        }
                        
                    })
                    ->addColumn('ganti_password', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#ganti_password'.$user->id.'" class="btn btn-primary">Ganti Password</button>
                        <!-- Modal-->
                        <div id="ganti_password'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="exampleModalLabel" class="modal-title">Ganti Password</h4>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <form action="'.route('admin.update_password',$user->id).'" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" required placeholder="Password Baru">
                                    </div>
                                    <div class="form-group">       
                                        <input type="submit" value="Update" class="btn btn-info">
                                    </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        ';
                    })
                    ->addColumn('ganti_foto', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#ganti_foto'.$user->id.'" class="btn btn-primary">Ganti Foto</button>
                        <!-- Modal-->
                        <div id="ganti_foto'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="exampleModalLabel" class="modal-title">Ganti Foto</h4>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <form action="'.route('admin.update_foto',$user->id).'" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <div class="form-group">
                                        <input type="file" name="pic" class="form-control" required value="'.$user->nama.'">
                                    </div>
                                    <div class="form-group">       
                                        <input type="submit" value="Update" class="btn btn-primary">
                                    </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                        ';
                    })
                    ->addColumn('ganti_status', function ($user) {
                        if ($user->status=="AKTIF") {
                            return '
                            <a href="'.route('admin.update_status',$user->id).'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i>'.$user->status.'</a>
                            ';
                        }else{
                            return '
                            <a href="'.route('admin.update_status',$user->id).'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i>'.$user->status.'</a>
                            ';
                        }
                        
                    })
                    ->escapeColumns([])
                    ->make(true);
    }
    
    public function input_admin(Request $request)
    {
        $request->validate([
            'nama'=>['required'], 
            'alamat'=>['required'],
            'email'=>['required','unique:users'],
            'hp'=>['required','unique:users'],
            'password'=>['required'],
          ]);

            User::create([
            'role_id'=>1, 
            'nama'=>$request->get('nama'), 
            'hp'=>$request->get('hp'), 
            'alamat'=>$request->get('alamat'),
            'email'=>$request->get('email'),
            'password'=>$request->get('password'),
            'sekolah'=>$request->get('sekolah'),
            'alamat_sekolah'=>$request->get('alamat_sekolah'),
            'status'=>"AKTIF",
            ]); 
        return redirect()->back()->with('success', 'Data Berhasil ditambahkan');
    }
    public function update_user(Request $request, $id)
    {
            $data=User::find($id);
            $data->nama = $request->get('nama');
            $data->hp = $request->get('hp');
            $data->alamat = $request->get('alamat');
            $data->email = $request->get('email');
            $data->password = $request->get('password');
            $data->sekolah = $request->get('sekolah');
            $data->alamat_sekolah = $request->get('alamat_sekolah');
            $data->save();
           
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');
    }
    public function update_password(Request $request, $id)
    {
            $data=User::find($id);
            $data->password = $request->get('password');
            $data->save();
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');
    }
    public function update_status($id)
    {
            $data=User::find($id);
            if ($data->status=="AKTIF") {
                $data->status = "PENDING";
            }else{
            $data->status = "AKTIF";
            }
            $data->save();
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');
    }
    public function update_foto(Request $request, $id)
    {
        $request->validate([
            'pic' => ['image','mimes:jpeg,png,jpg,gif,svg']
          ]);
            $data=User::find($id);
            $foto = $request->file('pic');
            $path = $foto->store('public/profile');
            Storage::delete($data->pic);
            $data->pic=$path;
            $data->save();
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');
    }
    public function delete_user($id)
    {
        $data=User::find($id);
        Storage::delete($data->pic);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil Dihapus');
    }
    
}
