<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function masuk(Request $request)
    {

        // request()->validate([
        // 'g-recaptcha-response' => 'required|captcha',
        // ]);
        
        $request->validate([
            'email'=>['required'],
            'password'=>['required']
          ]);
         $email=$request->get('email');
         $password=$request->get('password');
         $user_cek=User::where('email','=',$email)->count();
         if ($user_cek==0) {
            return redirect('/login')->with('gagal', 'Email Tidak Terdaftar');
         }else{
             
            $user=User::where('email','=',$email)->first();
            if ($user->status=="PENDING") {
                return redirect('/login')->with('gagal', 'Hubungi Admin Untuk Aktivasi');
            }else{
            $p_asli=$password;
            $p_hash=$user->password;
            //dd(Hash::check($p_asli, $p_hash));
            $cek=Hash::check($p_asli, $p_hash);
                if ($cek) {
                    Auth::guard('web')->loginUsingId($user->id);
                        //dd($role->role);
                       if ($user->role_id==1) {
                          return redirect('/admin')->with('success', 'Berhasil Masuk');
                       }else if ($user->role_id==2) {
                        return redirect('/guru')->with('success', 'Berhasil Masuk');
                     }else{
                        return redirect('/login')->with('gagal', 'Anda Buka Siapa Siapa!');
                    }
                }else{
                    return redirect('/login')->with('gagal', 'Password Anda Salah');
                }
            }
            
         }
    }
    public function keluar()
    {
        if (Auth::guard('web')->check()) {
            # code...
            Auth::guard('web','roles')->logout();
            return redirect('/login');
        }else{
            return redirect('/login');
        }
    }

}
