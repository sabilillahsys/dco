<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\posting;
use DataTables;
use Auth;
use Illuminate\Support\Facades\Storage;
class PostingController extends Controller
{
    public function data_post()
    {
        return view('admin.data_post');
    }
    public function json_post()
    {
        $data=posting::all();
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <a href="'.route('post.form_update_post',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Update</a>
                        <a href="'.route('post.delete_post',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                        ';
                    })
                    ->addColumn('pic', function ($user) {
                        $ub=$user->pic;
                        $url=url('/').Storage::url($user->pic);
                        $not= asset('img/logo.png');
                        $kondisi="belum";
                        if($ub == $kondisi){
                            return '
                            <img src="'.$not.'" width="113px" alt="">
                            ';
                        }else{
                            return '
                            <img src="'.$url.'" width="113px" alt="">
                            ';
                        }
                        
                    })
                    ->escapeColumns([])
                    ->make(true);
    }
    public function form_input_post()
    {
        return view('admin.form_input_post');
    }
    public function input_post(Request $request)
    {
        //
        $request->validate([
            'judul'=>['required'], 
            'isi'=>['required'],
            'pic'=>['required'],
          ]);
          $foto = $request->file('pic');
          $path = $foto->store('public/pic');
            posting::create([
            'user_id'=>Auth::User()->id, 
            'judul'=>$request->get('judul'), 
            'isi'=>$request->get('isi'), 
            'pic'=>$path,
            ]); 
        return redirect()->back()->with('success', 'Data Berhasil ditambahkan');
    }
    public function form_update_post($id)
    {
        $data=posting::find($id);
        return view('admin.form_update_post',compact('data'));
    }
    public function update_post(Request $request, $id)
    {
            $data=posting::find($id);
                $foto = $request->file('pic');
            if ($foto==null) {
                $data->judul = $request->get('judul');
                $data->isi = $request->get('isi');
                $data->save();
            }else{
                $path = $foto->store('public/profile');
                $data->judul = $request->get('judul');
                $data->isi = $request->get('isi');
                Storage::delete($data->pic);
                $data->pic=$path;
                $data->save();
            }
            
           
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');
    }

    public function delete_post($id)
    {
        //
        $data=posting::find($id);
        Storage::delete($data->pic);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil Dihapus');
    }
}
