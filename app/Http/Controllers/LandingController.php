<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\posting;
use DataTables;
use Auth;
use Illuminate\Support\Facades\Storage;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('login');
    }
    public function registrasi()
    {
        //
        return view('registrasi');
    }
    public function home()
    {
        //
        return view('index');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>['required'], 
            'alamat'=>['required'],
            'email'=>['required','unique:users'],
            'hp'=>['required','unique:users'],
            'password'=>['required'],
            'sekolah'=>['required'],
            'alamat_sekolah'=>['required'],
          ]);

            User::create([
            'role_id'=>2, 
            'nama'=>$request->get('nama'), 
            'hp'=>$request->get('hp'), 
            'alamat'=>$request->get('alamat'),
            'email'=>$request->get('email'),
            'password'=>$request->get('password'),
            'sekolah'=>$request->get('sekolah'),
            'alamat_sekolah'=>$request->get('alamat_sekolah'),
            'status'=>"PENDING",
            ]); 
   
        return redirect()->back()->with('success', 'Berhasil Ditambahkan');
    }
    public function view_post($id)
    {
        $data=posting::find($id);
        $data_all=posting::limit(3)->get();
        return view('post',compact('data','data_all'));
    }
    public function blog()
    {
        $data=posting::all();
        # code...
        return view('blog',compact('data'));
    }
}
