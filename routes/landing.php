<?php
    Route::get('/keluar', 'LoginController@keluar')->name('keluar');
    Route::post('/masuk', 'LoginController@masuk')->name('masuk');
    Route::get('/login', 'LandingController@index')->name('login');
    Route::get('/registrasi', 'LandingController@registrasi')->name('registrasi');
    Route::post('/store', 'LandingController@store')->name('store');
            
    Route::get('/','LandingController@home')->name('home');
    Route::get('/blog','LandingController@blog')->name('blog');
    Route::get('/view_post/{id}','LandingController@view_post')->name('view_post');
            