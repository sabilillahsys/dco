<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Admin']],function(){
        Route::group([
            'prefix' => 'admin','as' => 'admin.',
        ], function(){
            Route::get('/', 'AdminController@index')->name('admin');
            Route::get('/data_admin','AdminController@data_admin')->name('data_admin');
            Route::get('/json_admin','AdminController@json_admin')->name('json_admin');
            Route::post('/input_admin','AdminController@input_admin')->name('input_admin');
            Route::post('/update_user/{id}','AdminController@update_user')->name('update_user');
            Route::post('/update_foto/{id}','AdminController@update_foto')->name('update_foto');
            Route::get('/update_status/{id}','AdminController@update_status')->name('update_status');
            Route::get('/delete_user/{id}','AdminController@delete_user')->name('delete_user');
        
        });

    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Admin','Guru']],function(){
        Route::group([
            'prefix' => 'admin','as' => 'admin.',
        ], function(){
            Route::post('/update_password/{id}','AdminController@update_password')->name('update_password');
        });

    });
});

