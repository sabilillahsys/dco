<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Admin']],function(){
        Route::group([
            'prefix' => 'post','as' => 'post.',
        ], function(){
            Route::get('/', 'PostingController@index')->name('post');
            Route::get('/data_post','PostingController@data_post')->name('data_post');
            Route::get('/json_post','PostingController@json_post')->name('json_post');
            Route::post('/input_post','PostingController@input_post')->name('input_post');
            Route::get('/form_input_post','PostingController@form_input_post')->name('form_input_post');
            Route::post('/update_post/{id}','PostingController@update_post')->name('update_post');
            Route::get('/form_update_post/{id}','PostingController@form_update_post')->name('form_update_post');
            Route::get('/delete_postr/{id}','PostingController@delete_post')->name('delete_post');
        
        });

    });
});